import sys
import asyncio
import asyncssh
from client import run_client
from multiprocess import Pipe

try:
    parent_chan, child_chan = Pipe()
    parent_data, child_data = Pipe()
    pipe = [parent_chan, child_data]
    asyncio.get_event_loop().run_until_complete(run_client(pipe))
except (OSError, asyncssh.Error) as exc:
    sys.exit('SSH connection failed: ' + str(exc))
