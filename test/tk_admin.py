#!/home/david/.virtualenvs/networkclient/bin/python
import tkinter as tk


class GNCVisualApp(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.hi_there = tk.Button(self)
        self.hi_there["text"] = "Hello World\n (click me)"
        self.hi_there["command"] = self.say_hi
        # self.hi_there["color"] = "#RRGGBB"
        self.hi_there.pack(side="top")
        self.quit = tk.Button(
            self, text="QUIT", fg="red", command=root.destroy)
        self.quit.pack(side="bottom")

    def say_hi(self):
        print("Hi there, wveryone!")


root = tk.Tk()
app = GNCVisualApp(master=root)
# Windows Manager
app.master.title("Hola")
app.master.maxsize(1000, 500)
# Start program
app.mainloop()
