from tkinter import Tk, Toplevel, Menu, Frame, FALSE, END, Text, StringVar, N, S, E, W, RIGHT, Y, NSEW, CENTER, Button
from tkinter import messagebox
from tkinter import ttk
from settings import USER_COLOR, \
    WELCOME_MSG, WCM_TAG, IS_DEV, TABS, \
    BASE_GROUP, WIDTHS, UI_TITLE, \
    MAIN_MENU


def sendmsg(self, event):
    this_msg = self.msg.get()
    self.print_msg(this_msg)
    if IS_DEV:
        print("Se envía mensaje")
        print(this_msg)
        # msg_entry.set(StringVar())
        # text.insert(END, "\n" + msg.get())


def print_msg(self, msg):
    text.config(state='normal')
    text.insert(END, "\n" + msg, (tag))
    text.config(state='disabled')
    text.yview_pickplace("end")
    msg_entry.delete(0, END)

# SET ALIAS

# Define GUI

group = 'admin'
tag = "user:" + group

# size

width_1 = 50
width_2 = 20

root = Tk()
root.option_add('*tearOff', FALSE)

win = Toplevel(root)

menubar = Menu(win)
win["menu"] = menubar
win.title("GPS Network User Interface [GNUI]")
win.resizable(True, True)
win.iconify()
root.withdraw()

# Define grip control widget:

# sg = ttk.Sizegrip(win)

# Define menus

menu_file = Menu(menubar)
menu_edit = Menu(menubar)
helpmenu = Menu(menubar, name='help')

menubar.add_cascade(menu=menu_file, label='File', underline=0)
menubar.add_cascade(menu=menu_edit, label='Edit', underline=0)
menubar.add_cascade(menu=helpmenu, label='Help', underline=0)

menu_file.add_command(label='New', command='')
menu_file.add_command(label='Open...', command='')
menu_file.add_command(label='Close', command='')

# Define Notebook tabs
notebook = ttk.Notebook(win)

TAB = dict()
for tab in TABS:
    TAB[tab] = ttk.Frame(notebook)
    notebook.add(TAB[tab], text=tab)

notebook.grid(column=0, row=1)

# Define Areas

# Consola activa
chat_now = TAB[TABS[0]]

text_frame = ttk.Frame(chat_now)

text_frame.grid(column=0, row=0)
# Information frame:

# scrollbar
scroll_text = ttk.Scrollbar(text_frame)
scroll_text.pack(side=RIGHT, fill=Y)

text = Text(
    text_frame,
    width=60,
    height=50,
    cursor='cross',
    yscrollcommand=scroll_text.set)

text.insert(END, WELCOME_MSG, (WCM_TAG))
text.tag_configure(
    WCM_TAG,
    foreground=USER_COLOR['user']['fg'], font='times 18 bold italic',
    relief='groove', justify='center')

text.config(state='disabled')
text.pack()

scroll_text.config(command=text.yview)

###############################################
# Info column
###############################################
color_code_bg = "gainsboro"
info_frame = Frame(chat_now, bg=color_code_bg, width=width_2)
info_frame.grid(row=0, column=1, sticky=N)

# Connect to server
# Create button
# Values are in settings

conn_btn = Button(info_frame, text='Connectar\n a\n GNS', fg='red', command='')
# conn_btn.config(justify='CENTER')
conn_btn.grid(pady=20)

# Text settings
user_label = dict()
square_label = dict()

color_code = ttk.Label(
    info_frame,
    text="Código de colores",
    background=color_code_bg,
    font='times 14 bold')
color_code.grid(column=0, row=1)
count = 2
for group in USER_COLOR.keys():
    # Create label
    user_label[group] = ttk.Label(
        info_frame, text=group, width=width_2 - 1, background=color_code_bg)
    square_label[group] = ttk.Label(
        info_frame, text=" ", background=USER_COLOR[group]['fg'], width=1)
    user_label[group].grid(column=0, row=count)
    square_label[group].grid(column=1, row=count)
    count += 1
    text.tag_configure(
        'user:' + group,
        foreground=USER_COLOR[group]['fg'], font=USER_COLOR[group]['font'],
        relief=USER_COLOR[group]['relief'])

###############################################

###############################################

# scroll_text.grid(column=1, row=0)

# msg entry
msg = StringVar()
msg_entry = ttk.Entry(chat_now, width=53, cursor='xterm', textvariable=msg)
# First return
msg_entry.bind("<Return>", sendmsg)
# Numeric pad return
msg_entry.bind("<KP_Enter>", sendmsg)
msg_entry.grid(row=1, column=0, pady=2)

send_btn = ttk.Button(chat_now, text="Enviar", width=width_2, underline=0)
# Mouse first button
send_btn.bind("<Button-1>", sendmsg)
send_btn.grid(row=1, column=1, padx=4)

# Run windows


def on_closing():
    print("Se cierra la terminal GPS Network")
    # win.destroy()
    root.destroy()


win.protocol("WM_DELETE_WINDOW", on_closing)
root.mainloop()
