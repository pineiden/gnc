#!/usr/bin/wish
package require Tk

checkbutton .bold -text Bold -variable bold -anchor w
.bold configure -tristatevalue 1
set bold 3
checkbutton .italic -text Italic -variable italic -anchor w
checkbutton .underline -text Underline \
    -variable underline -anchor w
grid .bold -sticky ew -padx 5 -pady 8 -ipadx 3 -ipady 3
grid .italic -sticky ew -padx 5 -pady 8 -ipadx 3 -ipady 3
grid .underline -sticky ew -padx 5 -pady 8 -ipadx 3 -ipady 3
