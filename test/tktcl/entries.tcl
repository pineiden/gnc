#!/usr/bin/wish
package require Tk

proc this_name {} {
    global name
    set this_name name
}
label .label -text "File name: "
entry .entry -width 20 -relief sunken -bd 2 \
    -textvariable name
grid .label .entry -padx 1m -pady 2m
label .name -textvariable this_name
grid .name  -padx 1m -pady 2m

#
