USER_COLOR = dict(
    user=dict(
        fg='brown', font="times 12", relief='flat'),
    admin=dict(
        fg='blue', font="helvetica 14 bold", relief="groove"),
    server=dict(
        fg='red', font="helvetica 14 bold", relief='sunken'),
    monitor=dict(
        fg='green', font="arial 14 bold", relief='raised'),
    slave=dict(
        fg='orange', font="arial 14 bold", relief='ridge'))

UI_TITLE = "GPS Network User Interface [GNUI]"

WELCOME_MSG = "¡Bienvenid@ al sistema de adquisición de datos GPS!"

WCM_TAG = 'WELCOME'

IS_DEV = True

TABS = ['Consola', 'Histórico', 'Estatus', 'Settings']

BASE_GROUP = 'user'

# <-w1-> | <-w2->
WIDTHS = [50, 20]

MAIN_MENU = dict(
    m_file=dict(
        name="Archivos", values=['Nuevo', 'Abrir', 'Guardar', 'Cerrar']),
    m_edit=dict(
        name="Editar", values=[]),
    m_help=dict(
        name="Ayuda", values=[]))
