#!/usr/bin/wish
# package require Tk

# Not working now

proc Yset {widgets master sb_args} {
    if {$master eq "master"} {
        $sb set {expand}$args
        set wl [lrange $widgets 1 end]
    }else {
        set wl [lrange $widgets 0 0]
    }
    Yview $wl moveto [lindex $args 0]
}
#Called by the scrollbar
proc Yview {widgets args} {
    foreach w $widgets {
        $w  Yview {*}$args
    }
}

set widgets [list .lb1 .lb2]

[lindex $widgets 0] configure \
    -yscrollcommand [list Yset $widgets master .sb]
[lindex $widgets 1] configure \
    -yscrollcommand [list Yset $widgets master .sb]
#scrollbar .sb configure -command [list Yview $widgets]
