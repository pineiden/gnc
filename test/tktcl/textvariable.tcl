#!/usr/bin/wish
package require Tk

proc watch {name}{
    toplevel .watch
    label .watch.label -text "Value of \"$name\": "
    label .watch.value -textvariable $name
    grid .watch.label .watch.value -pady 12
}
