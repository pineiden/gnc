#!/usr/bin/wish
package require Tk

# Tearoff disabled on all menus
option add *Menu.tearOff 0

menu .m
. configure  -menu .m
.m add cascade -label File -menu .m.file -underline 1
.m add cascade -label Edit -menu .m.edit -underline 0
.m add cascade -label View -menu .m.view -underline 0
.m add cascade -label Graphics -menu .m.graphics -underline 0
.m add cascade -label Text -menu .m.text -underline 0
.m add cascade -label Help -menu .m.help -underline 0

menu .m.text
.m.text add checkbutton -label Bold -variable bold
.m.text add checkbutton -label Italic -variable italic
.m.text add checkbutton -label Underline -variable underline
.m.text add separator
.m.text add radiobutton -label Times -variable font -value Times
.m.text add radiobutton -label Helvetica -variable font -value Helvetica
.m.text add radiobutton -label Courier -variable font -value Courier
.m.text add separator
.m.text add command -label "Insert Bullet" -command "insertButllet"
.m.text add command -label "Margins and Tabs..." -command "mkMarginPanel"


menu .m.graphics

.m.graphics add cascade -label "Line Color" -menu .m.graphics.color -underline 5
.m.graphics add cascade -label "Line Width" -menu .m.graphics.width -underline 5

menu .m.graphics.color
.m.graphics.color add radiobutton -label "red" -variable lineWidth -value 0.25
.m.graphics.color add radiobutton -label "green" -variable lineWidth -value 0.25
.m.graphics.color add radiobutton -label "blue" -variable lineWidth -value 0.25

menu .m.graphics.width
.m.graphics.width add radiobutton -label "0.25 point" -variable lineWidth -value 0.25
