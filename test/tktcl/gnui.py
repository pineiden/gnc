from tkinter import Tk, Toplevel, Menu, Frame, FALSE, END, Text, StringVar, N, S, E, W, RIGHT, Y, NSEW, CENTER, Button
from tkinter import messagebox
from tkinter import ttk
from settings import USER_COLOR, \
    WELCOME_MSG, WCM_TAG, IS_DEV, TABS, \
    BASE_GROUP, WIDTHS, UI_TITLE, \
    MAIN_MENU


class GPSNetworkUI(object):

    # Define methods
    def __init__(self, *args, **kwargs):
        self.group = BASE_GROUP
        self.tag = "user:" + self.group
        self.widths = WIDTHS
        self.init_app()
        self.define_main_menu()
        self.app_tabs()
        self.build_tabs()
        self.msg_entry()
        self.info_column()
        self.close_protocol()

    def init_app(self):
        self.root = Tk()
        # modern menus
        self.root.option_add('*tearOff', FALSE)
        self.win = Toplevel(self.root)
        win = self.win
        win.title(UI_TITLE)
        win.resizable(True, True)
        win.iconify()
        self.root.withdraw()

    def define_main_menu(self):
        # Define menus
        win = self.win
        self.menubar = Menu(win)
        menubar = self.menubar
        win["menu"] = menubar
        # Declare menu list
        menu_file = Menu(menubar)
        menu_edit = Menu(menubar)
        helpmenu = Menu(menubar, name='help')

        menubar.add_cascade(
            menu=menu_file, label=MAIN_MENU['m_file']['name'], underline=0)
        menubar.add_cascade(
            menu=menu_edit, label=MAIN_MENU['m_edit']['name'], underline=0)
        menubar.add_cascade(
            menu=helpmenu, label=MAIN_MENU['m_help']['name'], underline=0)

        # Define every menu
        # menu_file
        for m in MAIN_MENU['m_file']['values']:
            # mainmenu(name_menu,option)
            menu_file.add_command(label=m, command='')

    def app_tabs(self):
        # Define Notebook tabs
        self.notebook = ttk.Notebook(self.win)
        notebook = self.notebook
        TAB = dict()
        for tab in TABS:
            TAB[tab] = ttk.Frame(notebook)
            notebook.add(TAB[tab], text=tab)

        self.TAB = TAB

        notebook.grid(column=0, row=1)

    def build_tabs(self):
        # Consola activa
        chat_now = self.TAB[TABS[0]]
        text_frame = ttk.Frame(chat_now)
        text_frame.grid(column=0, row=0)
        # Information frame:
        # scrollbar
        scroll_text = ttk.Scrollbar(text_frame)
        scroll_text.pack(side=RIGHT, fill=Y)
        text = Text(
            text_frame,
            width=60,
            height=50,
            cursor='cross',
            yscrollcommand=scroll_text.set)
        self.text = text
        text.insert(END, WELCOME_MSG, (WCM_TAG))
        text.tag_configure(
            WCM_TAG,
            foreground=USER_COLOR['user']['fg'], font='times 18 bold italic',
            relief='groove', justify='center')
        text.config(state='disabled')
        text.pack()
        scroll_text.config(command=text.yview)

    def info_column(self):
        ###############################################
        # Info column
        ###############################################
        chat_now = self.TAB[TABS[0]]
        text = self.text
        color_code_bg = "gainsboro"
        info_frame = Frame(chat_now, bg=color_code_bg, width=self.widths[1])
        info_frame.grid(row=0, column=1, sticky=N)
        # Connect to server
        # Create button
        # Values are in settings
        conn_btn = Button(
            info_frame, text='Connectar\n a\n GNS', fg='red', command='')
        # conn_btn.config(justify='CENTER')
        conn_btn.grid(pady=20)
        # Text settings
        user_label = dict()
        square_label = dict()

        color_code = ttk.Label(
            info_frame,
            text="Código de colores",
            background=color_code_bg,
            font='times 14 bold')
        color_code.grid(column=0, row=1)
        count = 2
        for group in USER_COLOR.keys():
            # Create label
            user_label[group] = ttk.Label(
                info_frame,
                text=group,
                width=self.widths[1] - 1,
                background=color_code_bg)
            square_label[group] = ttk.Label(
                info_frame,
                text=" ",
                background=USER_COLOR[group]['fg'], width=1)
            user_label[group].grid(column=0, row=count)
            square_label[group].grid(column=1, row=count)
            count += 1
            text.tag_configure(
                'user:' + group,
                foreground=USER_COLOR[group]['fg'],
                font=USER_COLOR[group]['font'],
                relief=USER_COLOR[group]['relief'])

    def msg_entry(self):
        # msg entry
        self.msg = StringVar()
        chat_now = self.TAB[TABS[0]]
        msg_entry = ttk.Entry(
            chat_now,
            width=self.widths[0],
            cursor='xterm',
            textvariable=self.msg)
        self.msg_entry = msg_entry
        # First return
        msg_entry.bind("<Return>", self.sendmsg)
        # Numeric pad return
        msg_entry.bind("<KP_Enter>", self.sendmsg)
        msg_entry.grid(row=1, column=0, pady=2)
        send_btn = ttk.Button(
            chat_now, text="Enviar", width=self.widths[1], underline=0)
        # Mouse first button
        send_btn.bind("<Button-1>", self.sendmsg)
        send_btn.grid(row=1, column=1, padx=4)

    def close_protocol(self):
        self.win.protocol("WM_DELETE_WINDOW", self.on_closing)

    def on_closing(self):
        print("Se cierra la terminal GPS Network")
        self.root.destroy()

    def loop(self):
        self.root.mainloop()

    def sendmsg(self, event):
        this_msg = self.msg.get()
        self.print_msg(this_msg)
        if IS_DEV:
            print("Se envía mensaje")
            print(this_msg)
            # msg_entry.set(StringVar())
            # text.insert(END, "\n" + msg.get())

    def print_msg(self, msg):
        text = self.text
        text.config(state='normal')
        text.insert(END, "\n" + msg, (self.tag))
        text.config(state='disabled')
        text.yview_pickplace("end")
        self.msg_entry.delete(0, END)

# SET ALIAS

GNUI = GPSNetworkUI

if __name__ == "__main__":
    gnui = GNUI()
    gnui.loop()
