#!/usr/bin/wish
package require Tk

button .ok -text OK 
button .apply -text Apply 
button .cancel -text Cancel -command cancel
button .help -text Help -bitmap question -command help
grid .ok .apply .cancel .help
