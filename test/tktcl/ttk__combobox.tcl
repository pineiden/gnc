ttk::combobox .cb
grid .cb
proc UpdateCombo {w} {
    set new [$w get]
    set values [$w cget -values]
    if {$new ni $values} {
        set values [linsert $values 0 $new]
        $w configure -values $values
    }
}

bind .cb <Return> {UpdateCombo %W}
