#!/usr/bin/wish
#package require Tk

proc watch {name} {
    toplevel .watch -padx 14 -pady 15
    labelframe .watch.lf -text "Value of \"$name\""
    label .watch.lf.value -textvariable $name
    grid .watch.lf.value -padx 3 -pady 3
    grid .watch.lf -sticky nsew
}
set country "USA"
watch country
