set score "Average score: 0.0"
set food 0
set ambiance 2
set service 0

# "Calculate an average of the ratings to display"
proc updateScore {val} {
    global food ambiance service score
    set average [expr {($food+$ambiance+$service)/3.0}]
    set score [format {Average score: %3.1f} $average]
}

scale .food -label "Food" -variable food \
    -length 8c -width .25c -from -5 -to 5 \
    -resolution 1 -tickinterval 1 -showvalue 1 \
    -orient horizontal -command {updateScore}
scale .ambiance -label "Ambiance" -variable ambiance \
    -length 8c -width .25c -from 0 -to 5 \
    -resolution 1 -tickinterval 1 -showvalue 0 \
    -orient horizontal -command {updateScore}
scale .service -label "Service" -variable service \
    -length 8c -width .25c -from 0 -to 5 \
    -resolution 1 -tickinterval 1 -showvalue 0 \
    -orient horizontal -command {updateScore}
label .score -textvariable score

grid .food -padx 2 -pady 2 -sticky w
grid .ambiance -padx 2 -pady 2  -sticky w
grid .service -padx 2 -pady 2 -sticky w
grid .score -padx 2 -pady 2 -sticky w
