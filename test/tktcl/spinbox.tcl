#!/usr/bin/wish
package require Tk

set months {January February March April May June July August September October November December}
label .label -text "Month: "
spinbox .spin -width 10 \
    -relief sunken -bd 5 \
    -textvariable mont \
    -state readonly \
    -values $months
grid .label .spin -padx 1m \
    -pady 2m

label .label2 -text "Font size: "
spinbox .spin2 -width 2 \
    -relief sunken -bd 2 \
    -textvariable size \
    -from 6 -to 72 \
    -increment 2 \
    -state normal
grid .label2 .spin2 -padx 1m \
    -pady 2m

label .label3 -text "Password: "
entry .passwd -show *
grid .label3 .passwd -sticky ew
