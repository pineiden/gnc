# -*- coding: utf-8 -*-

# Echo server program
import os
import socket
import asyncio
import math
import re
# These values are constant
from socket_conf import AF_TYPE, SOCK_TYPE
from socket_conf import HEADER
from socket_conf import ENDER
from socket_conf import sock
from socket_conf import gnc_path
from socket_conf import t_out
from socket_conf import buffsize
from socket_conf import uin
from socket_conf import char_code

# Asyncio guide:
# http://www.snarky.ca/how-the-heck-does-async-await-work-in-python-3-5

# More ref about socket:
# http://stackoverflow.com/questions/27014955/socket-connect-vs-bind

# example:

# Best example: https://gist.github.com/jamilatta/7603968

import uuid


def my_random_string(string_length=10):
    """Returns a random string of length string_length.

    :param string_length: a positive int value to define the random length
    :return:
    """
    a = 3
    assert string_length > a, "No es un valor positivo sobre " + str(a)
    random = str(uuid.uuid4())  # Convert UUID format to a Python string.
    random = random.upper()  # Make all characters uppercase.
    random = random.replace("-", "")  # Remove the UUID '-'.
    return random[0:string_length]  # Return the random string.
# str(my_random_string(6))
# print(my_random_string(6)) # For example, D9E50C


def complete_nro(val, character='0', n=3):
    s_val = str(val).upper()
    l = len(s_val)
    delta = n - l
    s_out = ''
    for k in range(delta):
        s_out += character
    s_out += s_val
    return s_out


class GNCSocket():
    """
    Socket class for GNC
    Create or connect to an UNIX socket
    Methods:
    * connect
    * send_msg
    * recv_msg
    * server
    * client
    """

    def __init__(self, mode='server'):
        self.bs = buffsize
        self.gnc_path = gnc_path
        if sock is None:
            if os.path.exists(self.gnc_path) and mode == 'server':
                os.remove(self.gnc_path)
            self.sock = socket.socket(AF_TYPE, SOCK_TYPE)
            self.status = 'ON'
        else:
            self.sock = sock
        # BIND: connect a path with socket
        # Print type
        # print(sock.SocketType)
        # self.sock.bind(path_gnc_socket)
        # Set timeout
        timeout = t_out
        self.conss = []
        self.addrs = []
        if AF_TYPE == socket.AF_INET or AF_TYPE == socket.AF_INET6:
            self.sock.settimeout(timeout)
        if mode == 'server':
            self.backlog = 3
            self.server()
        elif mode == 'client':
            self.client()
        else:
            print("Debe ejecutar metodo server o client ")
        # MSG Format
        # Defined:
        # LEN_HEAD MSG LCHAR(hex) LEN MSG END
        # Send format
        self.msg_struct = b"IDX hex(PAGE)/hex(TOT_PAGES) MSG hex(LEN) MSG END"
        self.msg_spaces = [x.start()
                           for x in re.finditer(b' ', self.msg_struct)]
        # IDX hex(PAGE)/hex(TOT_PAGES) MSG hex(LEN) MSG END
        # How to generate:
        # msg -> bytes
        # len(msg_bytes)
        # # envios
        # self.msg_limit = msg_limit
        # self.msg_limit_hex = str(hex(self.msg_limit)).split('x')[1]
        # self.nchar = len(self.msg_limit_hex)
        # Limit_msg = 512 (default), n=len(str(512))
        # To hex: 0x200
        # Split x
        # Get value and complete => LCHAR (en hex)
        self.header = HEADER
        self.ender = ENDER
        self.uin = uin
        self.idx = []
        self.conns = []
        self.addrs = []

    # a generator of a message list for send
    def generate_msg(self, msg):
        assert isinstance(msg, str), "No es string el mensaje"
        b_msg = msg.encode(char_code)
        T_LEN = len(b_msg)
        hex_T_LEN = str(hex(T_LEN)).split('x')[1]
        # Se obtiene el largo de caracteres como cota superior de largo de mensaje
        # A utilizar en paginacion
        new_n = len(hex_T_LEN)
        self.n_char = new_n
        self.base_len = len(self.header) + len(self.ender) + \
            len(self.msg_spaces) + self.uin + 3 * self.n_char + 1
        # Largo de pedazo de mensaje a enviar por partes
        self.len_msg = self.bs - self.base_len
        assert self.len_msg >= 1, "Debes definir un buffsize de mayor tamaño"
        bs = self.bs  # buffsize
        assert new_n + 1 < self.len_msg, "No es posible enviar mensaje"
        # Cantidad maxima de mensajes a enviar
        n_msgs = math.ceil(T_LEN / self.len_msg)
        hex_n_msgs = str(hex(n_msgs)).split('x')[1]
        # Cantidad de paginas
        self.N_n_msgs = len(str(hex_n_msgs))
        # hex_nmsgs = str(hex(n_msgs)).split('x')[1]
        header = self.header.encode(char_code)
        ender = self.ender.encode(char_code)
        # Se construye: hex(PAGE)/hex(TOT_PAGES) MSG hex(LEN) MSG END
        if n_msgs > 1:
            for i in range(n_msgs):
                # Construir header:
                # Conocer parte de msg a enviar
                # Conocer largo de msg_i
                # Cantidad carácteres largo
                step = i * self.len_msg
                this_page = str(hex(i + 1)).split('x')[1]
                str_page = complete_nro(
                    this_page, n=new_n) + "/" + complete_nro(
                        hex_n_msgs, n=new_n)
                page = str_page.encode(char_code)
                msg_i = b_msg[step:step + self.len_msg]
                msg_l = len(msg_i)
                L = len(str(msg_l))
                msg_len = msg_l + 2 + len(ender)
                hex_msg_len = complete_nro(
                    str(hex(msg_len).split('x')[1]), n=new_n)
                msg_header = header + b" " + page + b" " +\
                    str(hex_msg_len).encode(char_code)
                this_msg = msg_header + b" " + msg_i + b" " + ender
                yield this_msg

        elif n_msgs == 1:
            this_page = str(hex(1)).split('x')[1]
            str_page = complete_nro(
                this_page, n=new_n) + "/" + complete_nro(
                    hex_n_msgs, n=new_n)
            page = str_page.encode(char_code)
            msg_len = (T_LEN + 2 + len(ender))
            hex_msg_len = complete_nro(
                str(hex(msg_len).split('x')[1]), n=new_n)
            msg_header = header + b" " + page + b" " +\
                str(hex_msg_len).encode(char_code)
            this_msg = msg_header + b" " + b_msg + b" " + ender
            yield this_msg

    def gen_idx(self):
        IDX = my_random_string(self.uin)
        t = True
        while t:
            if IDX not in self.idx:
                self.idx.append(IDX)
                t = False
            else:
                IDX = my_random_string(self.uin)
        return IDX

    def send_msg(self, msg):
        conn = self.sock
        tot = self.N_n_msgs
        #assert tot == len(msg), "Hay un cruce de mensajes"
        IDX = self.gen_idx().encode(char_code)
        for b_msg in self.generate_msg(msg):
            # Here the splited messages
            # First value is header length
            # Catch them
            # 1+1+self.uin
            # Find first space to get index from header len value
            print("Enviando")
            print(b_msg)
            to_send = IDX + b" " + b_msg
            conn.send(to_send)

    def recv_msg(self):
        conn = self.conn
        # addr = self.addr
        bs_0 = 1
        count = 0
        b_header = b''
        idx_recv = ''
        t = True
        msg_tot = ''
        n_msgs_idx = 0
        # While n<=page[2]
        print("Recibiendo msg")

        while t:
            # 4 : len(spaces(header))
            while count < 4:
                # print(count)
                char_recv = conn.recv(bs_0)
                b_header += char_recv
                # print(b_header)
                if char_recv == b" ":
                    count += 1
                    # When 4 stop while
                # Msg header whit the last space
            header = b_header.decode(char_code)
            # find spaces:
            spaces = [x.start() for x in re.finditer(' ', header)]
            # betwen IDX and MSG
            sp_1 = spaces[0]
            # betwen MSG PGcount
            sp_2 = spaces[1]
            # hex(Pag_counter)
            sp_3 = spaces[2]
            s_page = header[sp_2 + 1:sp_3].split("/")
            # To list and int
            page = [int("0x" + s_page[0], 16), int("0x" + s_page[1], 16)]
            # betwen PGcount and MSG_LEN
            # last spaces, after MSG_LEN
            sp_4 = spaces[3]
            assert self.header == header[sp_1 + 1:
                                         sp_2], "No es un encabezado correcto"
            this_idx_recv = header[:sp_1]
            if idx_recv == '':
                idx_recv = this_idx_recv
                n_msgs_idx = 1
            else:
                n_msgs_idx += 1
            assert n_msgs_idx == page[
                0], "Error en mensaje, no coincide #idx con pagina " + str(
                    page[0])
            # b_msg = self.sock.recv(self.bs)
            # X
            lmsg = header[sp_3 + 1:sp_4]
            # Buffer length msg + 1 space
            bs_1 = int("0x" + lmsg, 16) + 1 + len(self.ender.encode(char_code))
            # Verify idx is new -> more msgs to join: count idx
            b_MSG = conn.recv(bs_1)
            l_ender = len(self.ender.encode(char_code))
            this_MSG = b_MSG[:-l_ender - 1].decode(char_code)
            this_ender = b_MSG[-l_ender:].decode(char_code)
            assert this_ender == self.ender, "No se ha terminado de \
            recibir el mensaje"

            msg_k = this_MSG
            msg_tot += msg_k
            # if no ENDMSG on END
            # call again
            if page[0] == page[1]:
                t = False
        self.msg_r = msg_tot
        return msg_tot

    def change_path(self, new_gnc_path):
        if os.path.exists(new_gnc_path):
            os.remove(new_gnc_path)
        self.gnc_path = new_gnc_path

    def connect(self):
        self.sock.connect_ex(self.gnc_path)
        self.mode = 'Client'

    def bind(self):
        self.sock.bind(self.gnc_path)
        self.status = 'ON'
        self.mode = 'Server'

    def listen(self):
        self.sock.listen(self.backlog)

    def set_backlog(self, new_backlog):
        assert isinstance(new_backlog,
                          int), "El nuevo backlog no es un valor válido"
        self.backlog = new_backlog

    def accept(self):
        conn, addr = self.sock.accept()
        self.conns.append(conn)
        self.addrs.append(addr)
        self.conn = conn
        self.addr = addr
        return (conn, addr)

    def list_clients(self):
        for i in range(len(self.conss)):
            print(self.addrs[i] + ":" + self.conns[i])

    def close(self):
        self.sock.close()
        self.status = 'OFF'

    def server(self):
        self.bind()
        self.listen()

    def client(self):
        self.connect()

    def get_name(self):
        return self.gnc_path

        # UNIX socket need a path

        # AF_UNIX is a constant, represent the address and protocol family
        # Feom socket man page
        # http://man7.org/linux/man-pages/man7/unix.7.html
        # is used to communicate betwen processes on SAME machine efficiently
        # So, the valid socket type: SOCK_STREAM

        # SOCK_STREAM
        # SOCK_STREAM     Provides sequenced, reliable, two-way, connection-
        #                 byte streams.  An out-of-band data transmission
        #                 mechanism may be supported.

        # Sockets of type SOCK_STREAM are full-duplex byte streams.  They do
        # preserve record boundaries.  A stream socket must be in a
        # state before any data may be sent or received on it.  A
        # to another socket is created with a connect(2) call.  Once
        # , data may be transferred using read(2) and write(2) calls
        # some variant of the send(2) and recv(2) calls.  When a session has
        # completed a close(2) may be performed.  Out-of-band data may
        # be transmitted as described in send(2) and received as described
        # recv(2).

        # communications protocols which implement a SOCK_STREAM ensure
        # data is not lost or duplicated.  If a piece of data for which
        # peer protocol has buffer space cannot be successfully transmitted
        # a reasonable length of time, then the connection is considered
        # be dead.  When SO_KEEPALIVE is enabled on the socket the protocol
        # in a protocol-specific manner if the other end is still alive.
        # SIGPIPE signal is raised if a process sends or receives on a broken
        # ; this causes naive processes, which do not handle the signal,
        # exit.  SOCK_SEQPACKET sockets employ the same system calls as
        # _STREAM sockets.  The only difference is that read(2) calls will
        # only the amount of data requested, and any data remaining in
        # arriving packet will be discarded.  Also all message boundaries
        # incoming datagrams are preserved.

        # From man socket: http://man7.org/linux/man-pages/man2/socket.2.html

        # PEP 383 https://www.python.org/dev/peps/pep-0383/ string path names

    # async def gnc_socket():
    # Listen on socket

    # While

    # Read from server

    # Read from socket

    # Send to socket

    # loop = asyncio.get_event_loop()

    # loop.run_until_complete(gnc_socket)

    # loop.close()
