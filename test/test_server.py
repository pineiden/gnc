from socket_server1 import GNCSocket
from socket_conf import TEST_TEXT
import sys

if __name__ == "__main__":
    mode = 'server'
    gs = GNCSocket(mode=mode)
    msg = TEST_TEXT

    # Testing message generator
    print("Status " + gs.status)
    for m in gs.generate_msg(msg):
        print(m.decode('utf-8'))
    # gs.send_msg(msg)
    gs.accept()
    # gs.send_msg(msg)
    print(gs.conn)
    # Testing communicate
    print(gs)
    print("Entrando a while")
    while True:
        try:
            datagram = gs.recv_msg()
            print("Datagram :")
            # if not datagram:
            # break
            # else:
            print(datagram)
            print("-" * 20)
            if "DONE" == datagram:
                break
        except KeyboardInterrupt as k:
            print("Cerrando con kb")
            gs.close()
            sys.exit()
    print("-" * 20)
    print("Apagando")
    gs.close()
    print("Listo!")
