from socket_server1 import GNCSocket
from socket_conf import TEST_TEXT
import sys

if __name__ == "__main__":
    mode = 'client'
    gs = GNCSocket(mode=mode)
    msg = TEST_TEXT

    # Testing message generator
    print("Status " + gs.status)
    for m in gs.generate_msg(msg):
        print(m.decode('utf-8'))
    # gs.send_msg(msg)
    # print(gs.conn)
    # Testing communicate
    # gs.accept()
    # print(gs.conn)
    while True:
        try:
            this_msg = input("->")
            #
            if this_msg != "":
                print("SEND :", this_msg)
                gs.send_msg(this_msg)
                if this_msg == "DONE":
                    print("Cerrando")
                    break
        except KeyboardInterrupt as k:
            print("Cerrando con kb")
            gs.close()
            sys.exit()
    print("-" * 20)
    print("Apagando")
    print("Listo!")
