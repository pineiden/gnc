from sqlalchemy import update
from sqlalchemy import Table, MetaData
try:
    from .db_session import session, engine, connection
except Exception:
    from db_session import session, engine, connection
try:
    from .models import Level, User, Command, Alert, Message, Log
except Exception:
    from models import Level, User, Command, Alert, Message, Log

import datetime

try:
    from .password import PasswordHash
except Exception:
    from password import PasswordHash

from passlib.hash import bcrypt


class SessionHandle(object):
    """
    A session middleware class to manage the basic elements in the database,
    has generic methods to verify the elements existence, update_ and obtain
    lists from tables
    """

    def __init__(self):
        self.session = session
        self.conn = connection
        self.metadata = MetaData()

    def exists_table(self, table_name):
        """
        Check if table_name exists on schema
        :param table_name: a table_name string
        :return: boolean {True,False}
        """
        return engine.dialect.has_table(engine.connect(), table_name)

    def exists_field(self, table_name, field_name):
        """
        Check if field exist in table
        :param table_name: table name string
        :param field_name: field name string
        :return:  bolean {True, False}
        """
        assert self.exists_table(table_name), 'No existe esta tabla'
        fields = Table(
            table_name, self.metadata, autoload=True, autoload_with=engine)
        r = [c.name for c in fields.columns]
        try:
            r.remove('id')
        except ValueError:
            pass
        assert field_name in r, 'No existe este campo en tabla'
        return True

    def value_type(self, table_name, field_name, value):
        """
        Check if value is the same value type in field
        :param table_name: table name string
        :param field_name: field name string
        :param value:  some value
        :return: boolean {True, False, None}; None if value type doesn't exist
        on that list, because there are only the most common types.
        """
        # get type value
        assert self.exists_table(table_name), 'No existe esta tabla'
        fields = Table(
            table_name, self.metadata, autoload=True, autoload_with=engine)
        r = [c.name for c in fields.columns]
        assert self.exists_field(table_name,
                                 field_name), 'Campo no existe en tabla'
        this_index = r.index(field_name)
        t = [str(c.type) for c in fields.columns]
        this_type = t[this_index]
        b = False
        if this_type == 'INTEGER' or this_type == 'BigInteger':
            assert isinstance(value, int)
            b = True
        elif this_type[
                0:
                7] == 'VARCHAR' or this_type == 'TEXT' or this_type == 'STRING':
            assert isinstance(value, str)
            b = True
        elif this_type == 'BOOLEAN':
            assert isinstance(value, bool)
            b = True
        elif this_type == 'DATE':
            assert isinstance(value, datetime.date)
            b = True
        elif this_type == 'DATETIME':
            assert isinstance(value, datetime.datetime)
            b = True
        elif this_type == 'FLOAT' or this_type == 'NUMERIC':
            assert isinstance(value, float)
            b = True
        else:
            b = None

        return b
        # check

    def update_table(self, table_name, instance, field_name, value):
        """
        Change some value in table
        :param table_name: table name class
        :param instance: instance to modify in database
        :param field: field name string
        :param value: value
        :return: void()
        """
        # update to database
        table = Table(
            table_name, self.metadata, autoload=True, autoload_with=engine)
        # print(table+":"+field_name+":"+value)
        up = update(table).where(
            table.c.id == instance.id).values({field_name: value})
        print(up)
        self.session.execute(up)
        self.session.commit()

    # LIST ELEMENTS

    def get_list(self, model):
        """
        Get the complete list of elements in some Model Class (Table in db)

        :param model:Model Class
        :return: a query list
        """
        return self.session.query(model).all()

# Class Alias
SH = SessionHandle


class SessionGNS(SH):
    """
    An specific SessionHandle instance who extends to database model in the GNS
    case, has level, user, command, alert, message and log handlers for generic
    cases.
    """

    # Create, update, delete level

    def level(self, this_value, group, description):
        """
        Create new level instance
        :param this_value: a value asigned to this particular level
        :param group: a name for this group
        :param description: a descrption
        :return: Level instance
        """
        level = Level(value=this_value, group=group, description=description)
        self.create_level(level)
        return level

    def create_level(self, level):
        """
        Create the level instance on database
        :param level:
        :return: void()
        """
        self.session.add(level)
        self.session.commit()

    def update_level(self, instance, fields, values):
        """
        Modify some fields from level instance identified by cid
        :param level: a table name
        :param fields: list of field names
        :param values:  list of field values
        :return: void()
        """
        t_name = 'level'
        assert len(fields) == len(values)
        for f in fields:
            v = values[fields.index(f)]
            self.update_table(t_name, instance, f, v)

    def delete_level(self, level):
        """
        Delete a level and flush database
        :param level: Level instance
        :return: void()
        """
        self.session.delete(level)
        self.session.flush()

    def get_levels(self):
        """
        Get a query list of levels
        :return:
        """
        return self.get_list(Level)

    def get_level_id(self, group):
        """
        Return a level id from table
        :param group:
        :return:
        """
        u = self.session.query(Level).filter_by(group=group).all()
        level_ = u[0]
        level_id = level_.id
        return level_id

    # create, update, delete user
    def user(self, name, password, pw_comx, level_id):
        """
        Create a new User instance
        :param name: name string
        :param password: password string, inside the method create the hash
        :param pw_comx: password complexity, int value
        :param level_id:
        :return: user instance
        """
        assert isinstance(pw_comx, int)
        if pw_comx < 12:
            pw_comx = 12

        passw_hash = PasswordHash.new(password, pw_comx)
        print(passw_hash)
        user = User(name=name, password=passw_hash, level_id=level_id)
        self.create_user(user)
        return user

    def create_user(self, user):
        """
        Save the user instance in database
        :param user: user instance
        :return: void()
        """
        self.session.add(user)
        self.session.commit()

    def update_user(self, instance, fields, values):
        """
        Modify user fields from user instance in
        database:
        :param instance: level instance
        :param fields: field name
        :param values: field value
        :return:
        """
        t_name = 'user'
        assert len(fields) == len(values)
        for f in fields:
            v = values[fields.index(f)]
            self.update_table(t_name, instance, f, v)

    def verify_password(self, user_name, password):
        """
        Check if password gived is ok
        :param user_name: string user name
        :param password: password as normal string
        :return:
        """
        u = self.session.query(User).filter_by(name=user_name).all()
        print(u)
        o_pw = u[0].password  # Password instance
        print(o_pw.hash)
        print(password)
        assert bcrypt.verify(password, o_pw)
        return True, u[0]

    def get_users(self):
        """
        Get users list
        :return: query list
        """
        return self.get_list(User)

    # create, delete command
    def command(self, name, command, description, level_id):
        """
        Create a new command instance

        :param name: name string
        :param command: command string
        :param description: description
        :param level_id: level int assigned
        :return: the Command instance
        """
        cmd = Command(
            name=name,
            command=command,
            description=description,
            level_id=level_id)
        self.create_command(cmd)
        return cmd

    def create_command(self, cmd):
        """
        Commit the command on the database

        :param cmd: Command instance
        :return: void()
        """
        self.session.add(cmd)
        self.session.commit()

    def update_command(self, instance, fields, values):
        """
        Change instace's fields from a command

        :param instance:
        :param fields:
        :param values:
        :return:
        """
        t_name = 'user'
        assert len(fields) == len(values)
        for f in fields:
            v = values[fields.index(f)]
            self.update_table(t_name, instance, f, v)

    def get_commands(self):
        """
        Get all list of commands
        :return: list of commands
        """
        return self.get_list(Command)

        # create, update, delete alert
    def alert(self, value, label, description):
        """
        Create a new alert instance and load on database

        :param value:
        :param description:
        :return:
        """
        alert = Alert(
            value=value,
            label=label,
            description=description, )
        self.create_alert(alert)
        return alert

    def create_alert(self, alert):
        """
        Take the new alert and load in database

        :param alert:
        :return:
        """
        self.session.add(alert)
        self.session.commit()

    def update_alert(self, instance, fields, values):
        """
        Modify some instance field from alert

        :param cid:
        :param fields:
        :param values:
        :return:
        """
        t_name = 'alert'
        assert len(fields) == len(values)
        for f in fields:
            v = values[fields.index(f)]
            self.update_table(t_name, instance, f, v)

    def get_alerts(self):
        """
        Return alerts instance list
        :return:
        """
        return self.get_list(Alert)

        # create, update, delete message
    def message(self, method, value, msg_id):
        """
        Create a new message to register the log

        :param method: method name
        :param value: value what cause log
        :param msg_id: msg identifier
        :return: message
        """
        message = Message(code=method, description=value, alert_id=msg_id)
        self.create_message(message)
        return message

    def create_message(self, message):
        """
        Save message on database
        :param message:
        :return:
        """
        self.session.add(message)
        self.session.commit()

    def update_message(self, instance, fields, values):
        """
        Modify some message instance
        :param instance: message instance
        :param fields: field name
        :param values: value for field name
        :return:
        """
        t_name = 'message'
        assert len(fields) == len(values)
        for f in fields:
            v = values[fields.index(f)]
            self.update_table(t_name, instance, f, v)

    def get_messages(self):
        """
        Return the lis of all messages
        :return:
        """
        return self.get_list(Message)

        # create, update, delete message

    def get_msg_id(self, type):
        u = self.session.query(Alert).filter_by(label=type).all()
        alert_ = u[0]
        alert_id = alert_.id
        return alert_id

    def logsession(self, mdate, method, value, msg_id):
        """
        Create a new log
        :param mdate: a Date stamp
        :param method: method name
        :param value:  value
        :param msg_id: msg id int
        :return:
        """
        message = Log(date=mdate, method=method, value=value, msg_id=msg_id)
        self.create_log(message)
        return message

    def create_log(self, message):
        """
        Save message instance on database
        :param message:
        :return:
        """
        self.session.add(message)
        self.session.commit()

    def update_log(self, instance, fields, values):
        """
        Modify some log instance
        :param instance: message instance
        :param fields: field name
        :param values: value for field name
        :return:
        """
        t_name = 'log'
        assert len(fields) == len(values)
        for f in fields:
            v = values[fields.index(f)]
            self.update_table(t_name, instance, f, v)

    def get_log(self):
        """
        Get all log in a list
        :return:
        """
        return self.get_list(Message)
