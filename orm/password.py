from passlib.hash import bcrypt
from sqlalchemy.ext.mutable import Mutable

class PasswordHash(Mutable):
    """
    Create a password hash with bcrypt
    """
    def __init__(self, hash_, rounds = None):
        assert len(hash_) == 60, 'bcrypt hash should be 60 schars'
        assert hash_.count('$'), 'bcrypt hash should have 3x "$"'
        self.hash = str(hash_)
        self.rounds = int(self.hash.split('$')[2])
        self.desired_rounds = rounds or self.rounds#catch the first

    def __eq__(self, candidate):
        """Hashes the candidate string and compares it to the stored hash."""
        if isinstance(candidate, str):
            if self.hash == bcrypt.encrypt(candidate, rounds = self.rounds):
                if self.rounds <  self.desired_rounds:
                    self._rehash(candidate)
                return True
        return False

    def __repr__(self):
        """Simple object representation."""
        return '<{}>'.format(type(self).__name__)

    @classmethod
    def coerce(cls, key, value):
        """Ensure that loaded values are PasswordHashes."""
        if isinstance(value, PasswordHash):
            return value
        return super(PasswordHash, cls).coerce(key, value)

    @classmethod
    def new(cls, password, rounds):
        """Creates a PasswordHash from the given password."""
        assert isinstance(password, str)
        return cls(bcrypt.encrypt(password, rounds =  rounds))


    @staticmethod
    def _new(password, rounds):
        """Returns a new bcrypt hash for the given password and rounds."""
        return bcrypt.encrypt(password, rounds =  rounds)

    def _rehash(self, password):
        """Recreates the internal hash and marks the object as changed."""
        self.hash = self._new(password, rounds = self.desired_rounds)
        self.rounds = self.desired_rounds
        self.changed()
