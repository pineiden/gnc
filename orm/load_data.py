from db_users import SessionGNS
from models import Level, User, Message
from pathlib import Path

import csv

pwd=str(Path(__file__).resolve().parent)

file_level = pwd+'/data/level_table.csv'
file_users = pwd+'/data/users_table.csv'
file_alert = pwd+'/data/alert_table.csv'
file_msg = pwd+'/data/log_table.csv'

session = SessionGNS()

this_level = dict()
with open(file_level, 'r') as level:
    reader = csv.DictReader(level, delimiter='|', quoting=csv.QUOTE_NONE)
    for row in reader:
        print(row)
        this_level.update(
            {
                int(row['value']):session.level(int(row['value']), row['group'], row['description'])
            }
        )
        #print(this_level[int(row['value'])])

this_user = dict()
with open(file_users, 'r') as users:
    reader = csv.DictReader(users, delimiter='|', quoting=csv.QUOTE_NONE)
    for row in reader:
        print(row)
        this_user.update(
            {
                row['user']: session.user(row['user'],row['pass'], 15, session.get_level_id(row['group']) )
            }
        )

this_alert =dict()
with open(file_alert, 'r') as alerts:
    reader = csv.DictReader(alerts, delimiter='|', quoting=csv.QUOTE_NONE)
    for row in reader:
        print(row)
        this_alert.update(
            {
                row['label']:session.alert(int(row['value']), row['label'], row['description'])
            }
        )
        #print(this_alert[row['label']])

this_msg=dict()
with open(file_msg, 'r') as msg:
    reader = csv.DictReader(msg, delimiter='|', quoting=csv.QUOTE_NONE)
    for row in reader:
        print(row)
        this_msg.update (
            {
                row ['msg_key']: session.message (row ['msg_key'], row ['msg'],
                                            session.get_msg_id (row ['type']))
            }
        )
        print(this_msg[row ['msg_key']])
