from sqlalchemy import create_engine

try:
    from .models import Base
except Exception:
    from models import Base

try:
    from .db_settings import db_path
except Exception:
    from db_settings import db_path


#create engine
engine = create_engine(db_path, echo=True)
#load schema on engine
Base.metadata.create_all(engine)