from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.orm import validates
from sqlalchemy import Column, Integer, Text, String, DateTime, ForeignKey
try:
    from .validators import isPositive
except Exception:
    from validators import isPositive
#call to pass column type

try:
    from .columns import Password
except Exception:
    from columns import Password


Base = declarative_base()

#Rama user level
class Level(Base):
    """
    Define level access to determine some command
    """

    __tablename__ = 'level'
    __table_args_ = {'schema':'users'}

    id = Column(Integer, primary_key = True, autoincrement=True)
    value = Column(Integer, unique=True)
    group = Column(String(12), unique=True)
    description = Column(Text)

    #back tables:
    user = relationship('User', back_populates='level')
    command = relationship('Command', back_populates='level')

    @validates('value')
    def validate_value(self, key, value):
        assert isPositive(value)
        return value

    def __str__(self):
        return str(self.value)+" :"+self.group+" :"+self.description

    def __repr__(self):
        return str(self.value)+" :"+self.group+" :"+self.description

class User(Base):
    """
    Define user to agroup in some level access
    """

    __tablename__ = 'user'
    __table_args_ = {'schema':'users'}

    id = Column(Integer, primary_key = True, autoincrement=True)
    name = Column(String(20), unique=True)
    password = Column(Password(rounds=13))
    level_id = Column(Integer, ForeignKey('level.id'))

    level = relationship('Level', back_populates='user')

    @validates('password')
    def _validate_password(self, key, password):
        return getattr(type(self), key).type.validator(password)

    def __str__(self):
        return self.name+" :"+str(self.level_id)

    def __repr__(self):
        return self.name+" :"+str(self.level_id)

#print 'Access granted'

class Command(Base):
    """
    Define what commands the users can do throw the server
    """
    __tablename__ = 'command'
    __table_args_ = {'schema':'users'}

    id = Column(Integer, primary_key = True, autoincrement=True)
    name = Column(String(20))
    command = Column(String(5))
    description = Column(Text)
    level_id = Column(Integer, ForeignKey('level.id'))

    level = relationship ('Level', back_populates='command')

    def __str__(self):
        return self.name+" :"+self.command

    def __repr__(self):
        return self.name+" :"+self.command

#Rama logging

class Alert(Base):
    """
    Define level alert
    """
    __tablename__ = 'alert'
    __table_args_ = {'schema':'logging'}

    id = Column (Integer, primary_key=True, autoincrement=True)
    value =  Column(Integer, unique=True)
    label = Column(String(15))
    description = Column(Text)

    level = relationship('Message', back_populates='alert')

    @validates ('value')
    def validate_value (self, key, value):
        assert isPositive (value)
        return value

    def __str__(self):
        return str(self.value)+" :"+self.description

    def __repr__(self):
        return str(self.value)+" :"+self.description

class Message(Base):
    """
    Define messages associated to some action, method or command.
    """
    __tablename__ = 'message'
    __table_args_ = {'schema':'logging'}

    id = Column(Integer, primary_key = True, autoincrement=True)
    code = Column(String(5))
    description = Column(Text)
    alert_id = Column(Integer, ForeignKey('alert.id'))

    alert = relationship ('Alert', back_populates='level')
    log = relationship ('Log', back_populates='msg')

    def __str__(self):
        return self.code+" :"+self.description


    def __repr__(self):
        return self.code + " :" + self.description


class Log(Base):
    """
    Define the log for the server working
    """

    __tablename__ = 'log'
    __table_args_ = {'schema':'logging'}

    id = Column (Integer, primary_key=True, autoincrement=True)
    date = Column(DateTime)
    method = Column(String(20))
    value = Column(Text)
    msg_id = Column(Integer, ForeignKey('message.id'))

    msg = relationship ('Message', back_populates='log')

    def __str__(self):
        return self.date+" :"+self.method+" :"+self.value+" :"+self.msg_id.code


    def __repr__(self):
        return self.date + " :" + self.method + " :" + self.value + " :" + self.msg_id.code