import sys
import asyncio
import asyncssh
from client import run_client
from multiprocess import Pipe
from multiprocessing import Pool

async def suma(a, b):
    c = a + b
    print(c)
    await c


def test():
    t_loop = asyncio.new_event_loop()
    a = t_loop.run_until_complete(suma(3, 4))
    print(a)


def conn(pipe):
    try:
        asyncio.get_event_loop().run_until_complete(run_client(pipe))
    except (OSError, asyncssh.Error) as exc:
        sys.exit('SSH connection failed: ' + str(exc))


def chan_print(pipe):
    try:
        chan = pipe.recv()
        print("Chann recibido:")
        print(chan)

    except:
        print("Chan no recibido en chan_print")


def mainloop():
    parent_chan, child_chan = Pipe()
    parent_data, child_data = Pipe()
    pipe = [parent_chan, child_data]
    pool = Pool(processes=3)
    pool.apply_async(test)
    pool.apply_async(conn, ([parent_chan, child_data],))
    pool.apply_async(chan_print, (child_chan,))
    pool.close()
    pool.join()


if __name__ == '__main__':
    mainloop()
