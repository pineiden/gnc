# ORM ASQLALCHEMY Database Scheme
# create engine
# from sqlalchemy import create_engine
# create session
# from sqlalchemy.orm import sessionmaker
# from orm.models import Level, User, Command, Alert, Message, Log
# from orm.db_settings import db_path

try:
    from conf.settings import SERVER, USER, PASSWORD, SEP
except Exception:
    from .conf.settings import SERVER, USER, PASSWORD, SEP

from gns.conf.settings import LIST_COMMANDS
from gns.library import context_split
# Cliente Asyncssh
# Connect to Server and attend messages, send messages
# Save messages to database
# Record log
# Filter command

from multiprocess import Pipe
# First: Define class client
import asyncio
import asyncssh
import sys
# Identify user group

# from socket import socket


class GPSNetworkClientSession(asyncssh.SSHClientSession):

    def session_started(self):
        self.on_session = True
        self.status = 'ON'
        print("Se inicia sesión")

    def pause_writing(self):
        """
        Called when buffer becomes full

        """
        asyncio.sleep(.005)
        pass

    def resume_writing(self):
        """
        Called when buffer has sufficiently drained
        """
        pass

        self._input = []
        self.idx = ''

    def connection_lost(self, exc):
        self.on_session = False
        print("Conexión perdida con GNS")
        self.status = 'OFF'
        if exc:
            print("SSH session error: " + str(exc), file=sys.stderr)

    def auth_completed(self):
        print("Auth completed")

GNCS = GPSNetworkClientSession


class GPSNetworkClient(asyncssh.SSHClient):

    def add_pipe(self, pipe):
        # transfer chan object
        assert len(pipe) == 2, "NO es de largo 2"
        print("Asignando pipes")
        self.in_chan = pipe[0]
        self.in_data = pipe[1]
        print(self._conn)
        while True:
            if self.is_conn == 'OK':
                # self.in_chan.send(self._conn)
                # transger data from gns
                break

    def session_started(self):
        print("Session cliente On")

    def connection_made(self, conn):
        print("Realizando conexion CONN")
        self._conn = conn
        print(self._conn.get_extra_info('peername')[0])
        self.is_conn = 'OK'

    def auth_completed(self):
        print("Auth completed")

    def connection_lost(self, exc):
        print("Conexion perdida")
        print(exc)

    def data_received(self, data, datatype):
        # print("Channel client session up")
        # this _input only has the last value received
        # the another values sended to the client log => TODO: Create client
        # log
        print("Data received")
        self._input = data.strip()
        # As msg data
        info = context_split(context_split(self._input)[2])
        print(info)
        print(data, end='')
        try:
            self.in_data.send(data)
        except:
            print("Aun no se crea pipe para envio de data")
        # ONLY RECEIVE SOME MESSAGES WITH DEFINED STRUCTURE
        if info[0] == 'INFO':
            if info[1] == 'SND' and info[2] == 'ID':
                self.idx = info[0]
                print("Session id, your [IDX]")
                print(self.idx)

GNC = GPSNetworkClient

async def run_client(pipe):
    conn, client = await asyncssh.create_connection(GNCS, SERVER['ip'],
                                                    port=SERVER['port'])
    async with conn:
        chan, session = await conn.create_session(GNC, '')
        writer, reader, error = await conn.open_session()
        writer.channel.write("Hola GNS")
        print("Conexión creada")
        session.add_pipe(pipe)
        print("Conexion ok enviando CHANN")
        print(chan)
        await chan.wait_closed()
