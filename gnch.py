from gnce import GPSNetworkChannel
from sock_server import ServerSocket
from multiprocessing import Process, Pipe, Queue
import asyncio


class Channel(object):
    def __init__(self):
        # async Queues
        queue_gnc = asyncio.Queue()
        queue_gus = asyncio.Queue()

        # multiprocess Pipes
        parent_flag_gnc, child_flag_gnc = Pipe()
        parent_flag_gus, child_flag_gus = Pipe()

        self.GNCCh = GPSNetworkChannel(parent_flag_gnc, child_flag_gus,
                                       queue_gus, queue_gnc)
        self.server = ServerSocket(parent_flag_gus, child_flag_gnc, queue_gnc,
                                   queue_gus)

    def mainloop(self):
        p_gnch = Process(target=self.GNCCh.mainloop)
        p_gus = Process(target=self.server.data_exchange)
        p_gnch.daemon = True
        p_gus.daemon = True
        p_gus.start()
        p_gnch.start()
        p_gus.join()
        p_gnch.join()


if __name__ == '__main__':
    channel = Channel()
    channel.mainloop()
