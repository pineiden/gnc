# A simple shh client with parallel input/output
from client import GNC, GNCS, SERVER,\
    LIST_COMMANDS, SEP

from gns.library import pattern_value, fill_pattern, context_split

import asyncio
import asyncssh
import sys

import concurrent.futures


async def send_msg(queue_reader, writer, error, end):
    # for i in range(4):
    #    msg_conn = await read(reader, end)
    #    print(msg_conn)
    # await read_loop(reader, end,)

    try:
        if not queue_reader.empty():
            queue_reader.join()
            for i in range(queue_reader.qsize()):
                msg_in = await queue_reader.get()
                print(msg_in)
            queue_reader.task_done()
        this_msg = input("->")
        #
        if this_msg != "":

            print("SEND :", this_msg)
            try:
                writer.write(this_msg)
                print("Ya enviado: " + this_msg)
            except Exception as exec:
                print(exec)
            if this_msg == "DONE":
                print("Cerrando server")
    except Exception as exec:
        print(exec)
    except KeyboardInterrupt as k:
        print("Cerrando con kb")
        sys.exit()


def write_in_thread(reader_queue, writer, error, end, loop):
    asyncio.set_event_loop(loop)
    future = asyncio.gather(
        *(asyncio.ensure_future(send_msg(reader_queue, writer, error, end))))
    loop.run_until_complete(future)


def read_in_thread(callback, end, queue, loop):
    asyncio.set_event_loop(loop)
    future = asyncio.gather(
        *(asyncio.ensure_future(read_loop(callback, end, queue))))
    loop.run_until_complete(future)


async def read_loop(callback, end, queue):
    for i in range(3):
        asyncio.sleep(.1)
        try:
            msg = await read(callback, end)
            if msg is not '':
                await queue.put(msg)
        except Exception as exec:
            print(exec)

async def read(callback, end):
    msg = ''
    n = 0
    while True:
        one_char = await callback.read(n=1)
        msg += one_char
        n = len(end)
        if len(msg) > n:
            if msg[-n::] == end and msg[-n - 1] is not '\\':
                break
    return msg[:-n]

async def run_client(loop):
    conn, client = await asyncssh.create_connection(GNCS,
                                                    SERVER['ip'],
                                                    port=SERVER['port'])
    async with conn:
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)
        end = '<end>'
        queue = asyncio.Queue()
        writer, reader, error = await conn.open_session()
        print([writer, reader, error])
        # RUN BOTH IN 2 THREADS:
        while True:
            try:
                await loop.run_in_executor(executor, read_in_thread,
                                           *(reader, end, queue, loop))
            except Exception as exec:
                print(exec)
            asyncio.sleep(.5)
            try:
                await loop.run_in_executor(executor, write_in_thread,
                                           *(queue, writer, error, end, loop))
            except Exception as exec:
                print(exec)

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run_client(loop))
    loop.close()
