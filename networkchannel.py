import asyncio
import asyncssh

from .constants import DEFAULT_LANG, DISC_PROTOCOL_ERROR, EXTENDED_DATA_STDERR
from .constants import MSG_CHANNEL_OPEN, MSG_CHANNEL_WINDOW_ADJUST
from .constants import MSG_CHANNEL_DATA, MSG_CHANNEL_EXTENDED_DATA
from .constants import MSG_CHANNEL_EOF, MSG_CHANNEL_CLOSE, MSG_CHANNEL_REQUEST
from .constants import MSG_CHANNEL_SUCCESS, MSG_CHANNEL_FAILURE
from .constants import OPEN_CONNECT_FAILED, PTY_OP_RESERVED, PTY_OP_END
from .constants import OPEN_REQUEST_PTY_FAILED, OPEN_REQUEST_SESSION_FAILED
from .editor import SSHLineEditorChannel, SSHLineEditorSession
from .misc import ChannelOpenError, DisconnectError, map_handler_name
from .packet import Boolean, Byte, String, UInt32, SSHPacketHandler


class GPSNetworkClientChannel(asyncssh.SSHClientChannel):

    def __init__(self):
        super.__init__()

    async def create(self, session_factory, command, subsystem, env,
                     term_type, term_size, term_modes, agent_forwarding, pipe):
        # pipe is list [in_chan,in_data]
        packet = await self._open(b'session')

        # Client sessions should have no extra data in the open confirmation
        packet.check_end()
        self._session = session_factory()

        # line added
        self._session.add_pipe(pipe[0], pipe[1])

        self._session.connection_made(self)

        for name, value in env.items():
            self._send_request(b'env', String(str(name)), String(str(value)))

        if term_type:
            if not term_size:
                width = height = pixwidth = pixheight = 0
            elif len(term_size) == 2:
                width, height = term_size
                pixwidth = pixheight = 0
            elif len(term_size) == 4:
                width, height, pixwidth, pixheight = term_size
            else:
                raise ValueError('If set, terminal size must be a tuple of '
                                 '2 or 4 integers')

            modes = b''
            for mode, value in term_modes.items():
                if mode <= PTY_OP_END or mode >= PTY_OP_RESERVED:
                    raise ValueError('Invalid pty mode: %s' % mode)

                modes += Byte(mode) + UInt32(value)

            modes += Byte(PTY_OP_END)

            if not (await self._make_request(b'pty-req',
                                             String(term_type),
                                             UInt32(width),
                                             UInt32(height),
                                             UInt32(pixwidth),
                                             UInt32(pixheight),
                                             String(modes))):
                self.close()
                raise ChannelOpenError(OPEN_REQUEST_PTY_FAILED,
                                       'PTY request failed')

        if agent_forwarding:
            self._send_request(b'auth-agent-req@openssh.com')

        if command:
            result = await self._make_request(b'exec', String(command))
        elif subsystem:
            result = await self._make_request(b'subsystem',
                                              String(subsystem))
        else:
            result = await self._make_request(b'shell')

        if not result:
            self.close()
            raise ChannelOpenError(OPEN_REQUEST_SESSION_FAILED,
                                   'Session request failed')

        self._session.session_started()
        self.resume_reading()

        return self, self._session
